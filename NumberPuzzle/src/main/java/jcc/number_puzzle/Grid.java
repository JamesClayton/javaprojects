package jcc.number_puzzle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

class Grid {
	
	public static final String NEW_LINE = System.getProperty("line.separator");

	private static long TALLY = 0;
	private Map<Integer, SolutionCell> SOLUTIONS = new TreeMap<Integer, SolutionCell>();
	
	private boolean preSolve = true;
	private boolean preRestrict = true;
	
	private List<RowOrColumn> lines = new ArrayList<RowOrColumn>();
	
	private List<RowOrColumn> rows = new ArrayList<RowOrColumn>();
	private List<RowOrColumn> columns = new ArrayList<RowOrColumn>();
	private List<RowOrColumn> diagonals = new ArrayList<RowOrColumn>();
	
	private List<TestCell> unknowns = new ArrayList<TestCell>();
	
	public Grid(boolean preSolve, boolean preRestrict, int[] rowExpected, int[] columnExpected, int[] diagsExpected, int[][] gridData) {
		this.preSolve = preSolve;
		this.preRestrict = preRestrict;
		int rowNumb = 0;
		int numb = 0;
		for(int row = 0; row < rowExpected.length; row++) {
			rowNumb++;
			numb++;
			RowOrColumn newRow = new RowOrColumn(rowExpected[row], rowNumb, "Row", numb);
			rows.add(newRow);
			lines.add(newRow);
		}
		int colNumb = 0;
		for(int col = 0; col < columnExpected.length; col++) {
			colNumb++;
			numb++;
			RowOrColumn newCol = new RowOrColumn(columnExpected[col], colNumb, "Column", numb);
			columns.add(newCol);
			lines.add(newCol);
		}
		boolean hasDiags = diagsExpected.length == 2 && diagsExpected[0] > 0 && diagsExpected[1] > 0; 
		if (hasDiags) {
			numb++;
			diagonals.add(new RowOrColumn(diagsExpected[0], 1, "Diagonal", numb));
			numb++;
			diagonals.add(new RowOrColumn(diagsExpected[1], 2, "Diagonal", numb));
		}
		lines.addAll(diagonals);
		createCells(gridData, rowExpected.length, hasDiags);
	}

	private void checkMyLines(RowOrColumn[] myLines) {
		TALLY++;
		System.out.print(".");
		for (RowOrColumn rOrC: myLines) {
			if (! rOrC.checkSum()) {
				return;
			}
		}
		dumpFullSolution();
	}
	private void dumpPuzzle() {
		StringBuilder bob = new StringBuilder();
		bob.append("Puzzle:");
		for (RowOrColumn rowOrCol: rows) {
			bob.append("\n\tRow ")
				.append(rowOrCol.number)
				.append(" [");
			Iterator<Cell> cellItty = rowOrCol.cells.iterator();
			while (cellItty.hasNext()) {
				Cell cell = cellItty.next();
				bob.append(cell.value);
				if (cellItty.hasNext()) {
					bob.append(", ");
				}
			}
			bob.append("] = ")
				.append(rowOrCol.expected);
		}
		for (RowOrColumn rowOrCol: columns) {
			bob.append("\n\tColumn ")
				.append(rowOrCol.number)
				.append(" [");
			Iterator<Cell> cellItty = rowOrCol.cells.iterator();
			while (cellItty.hasNext()) {
				Cell cell = cellItty.next();
				bob.append(cell.value);
				if (cellItty.hasNext()) {
					bob.append(", ");
				}
			}
			bob.append("] = ")
				.append(rowOrCol.expected);
		}
		for (RowOrColumn rowOrCol: diagonals) {
			bob.append("\n\tDiagonal ")
				.append(rowOrCol.number)
				.append(" [");
			Iterator<Cell> cellItty = rowOrCol.cells.iterator();
			while (cellItty.hasNext()) {
				Cell cell = cellItty.next();
				bob.append(cell.value);
				if (cellItty.hasNext()) {
					bob.append(", ");
				}
			}
			bob.append("] = ")
				.append(rowOrCol.expected);
		}
		log(bob.toString());	

	}
	private void dumpFullSolution() {
		Iterator<SolutionCell> itty = SOLUTIONS.values().iterator();
		int numberOfSolutions = (itty.hasNext()) ? itty.next().solutions.size() : 0;
		numberOfSolutions++;
		StringBuilder bob = new StringBuilder();
		bob.append("Found solution #")
			.append(numberOfSolutions)
			.append(" : ");
		for (RowOrColumn row: rows) {
			Iterator<Cell> cellItty = row.cells.iterator();
			while (cellItty.hasNext()) {
				Cell cell = cellItty.next();
				if (cell instanceof TestCell) {
					TestCell testCell = (TestCell) cell;
					SolutionCell solCell = SOLUTIONS.get(cell.cellNumber);
					if (solCell == null) {
						solCell = new SolutionCell(cell.rowIndex, cell.colIndex, cell.cellNumber);
						SOLUTIONS.put(cell.cellNumber, solCell);
					}
					solCell.addSolution(testCell);
					bob.append(cell)
						.append(", ");
				}
			}
		}		
		log(bob.toString());	
	}
	void log(String mess) {
		NumberGridSolver.SOLVER.log(mess);
	}
	private void createCells(int[][] gridData, int numberOfRows, boolean hasDiags) {
		int cellNumb = 0;
		for(int rowIndex = 0; rowIndex < numberOfRows; rowIndex++) {
			int[] rwoData = gridData[rowIndex];
			for(int colIndex = 0; colIndex < rwoData.length; colIndex++) {
				int cellVal = rwoData[colIndex];
				Cell cell = null;
				cellNumb++;
				if (cellVal == -1)  {
					cell = new TestCell(rowIndex, colIndex, cellNumb);
					unknowns.add((TestCell) cell);
				}
				else {
					cell = new Cell(rowIndex, colIndex, cellVal, cellNumb);
				}
				RowOrColumn row = rows.get(rowIndex);
				row.addCell(cell);
				RowOrColumn column = columns.get(colIndex);
				column.addCell(cell);
				if (hasDiags) {
					if (rowIndex == colIndex) {
						RowOrColumn diag1 = diagonals.get(0);
						diag1.addCell(cell);
					}
					if (rowIndex + colIndex == numberOfRows - 1) {
						RowOrColumn diag2 = diagonals.get(1);
						diag2.addCell(cell);
					}
				}
			}
		}
	}
	Map<Integer, SolutionCell> crunch() throws NoValidSolutionsException {
		dumpPuzzle();
		TALLY = 0;
		int unStart = unknowns.size();
		String mess = "Started Crunch. Number of Unknowns: " + unknowns.size();
		log(mess);
		
		preCrunch();
		
		TALLY = 0;
		
		StringBuilder bob = new StringBuilder();
		for (TestCell cell: unknowns) {
			cell.dumpData(bob, true);
		}
		
		log(bob.toString());

		bob = new StringBuilder();
		dumpUnknownData(bob);
		System.out.println(bob.toString());

		mess = "Preloops cut down finished. Number of Unknowns: " + unknowns.size() + " was: " + unStart;
		log(mess);

		Collections.sort(unknowns);
		TestCell[] passIton = unknowns.toArray(new TestCell[unknowns.size()]);
		RowOrColumn[] myLines = lines.toArray(new RowOrColumn[lines.size()]);
		loop(myLines, passIton, passIton.length, 0);
		
		int noSols = 0;
		if (! SOLUTIONS.isEmpty()) {
			SolutionCell anyCell = SOLUTIONS.values().iterator().next();
			noSols = anyCell.solutions.size();
		}
		
		bob = new StringBuilder();
		bob.append("All SOLUTIONS ~solutions: ")
			.append(noSols);
		int i = 1;
		for (SolutionCell sol: SOLUTIONS.values()) {
			bob.append(NEW_LINE)
				.append("\t")
				.append(sol)
				.append(" = B(")
				.append(i++)
				.append(")");
		}
		log(bob.toString());
		return SOLUTIONS;
	}
	private void dumpUnknownData(StringBuilder bob) {
		for (RowOrColumn line: lines) {
			if (line.numberOfUnknowns() > 0) {
				line.dumpData(bob);
			}
		}		
	}

	private void loop(RowOrColumn[] myLines, TestCell[] allUnknowns, int noUnknows, int index) {
		
		if (index == noUnknows) {
			checkMyLines(myLines);
			return;
		}
		else {
			TestCell myUnknown = allUnknowns[index];
			int min = myUnknown.minValue();
			int max = myUnknown.maxValue() + 1;
			Iterator<Integer> itty = myUnknown.iterator();
			while (itty.hasNext()) {
				int u1 = itty.next();
//			for (int u1 = min; u1 < max; u1++) {
				boolean carryOn = myUnknown.setAndTest_TestValue(u1);
				if (carryOn) {
					if (index == 0) {
						System.out.println(new Date() + "Setting test value in first unknown: " + myUnknown);
					}
					int nextInt = index + 1;
					loop(myLines, allUnknowns, noUnknows, nextInt);
				}
				myUnknown.resetTestValue();
			}
		}

	}

	private void preCrunch() throws NoValidSolutionsException {	
		boolean carryOn = true;
		while (carryOn) {
			carryOn = preloop_Solve();
			if (! carryOn) {
				carryOn = preloop_Restrict();
			}
		}
		removeSolvedLines();
	}
	
	private void removeSolvedLines() {
		List<RowOrColumn> solvedLines = new ArrayList<RowOrColumn>();
		for (RowOrColumn rOrC: lines) {
			if (rOrC.numberOfUnknowns() == 0) {
				solvedLines.add(rOrC);
			}
		}
		lines.removeAll(solvedLines);
	}
	private boolean preloop_Restrict() throws NoValidSolutionsException {
		if (preRestrict) {
			for (RowOrColumn row: lines) {
				row.preloop_Restrict();
			}			
		}
		if (preSolve) {
			List<Cell> solved = new ArrayList<Cell>();
			for (TestCell unknown: unknowns) {
				if (unknown.maxValue() == unknown.minValue()) {
					unknown.setSolvedValue(unknown.minValue());
					solved.add(unknown);
					String mess = "Solved a cell: " + unknown + " (min and max range are equal)";
					System.out.println(mess);
				}
			}
			if (solved.isEmpty()) {
				return false;
			}
			else {
				unknowns.removeAll(solved);
				return true;
			}
		}
		return false;	
	}
	private boolean preloop_Solve() throws NoValidSolutionsException {
		// Number of unknowns BEFORE
		int unkns_Before = unknowns.size();
		
		String mess = "Presolve: Number of Unknowns: " + unknowns.size();
		log(mess);

		
		for (RowOrColumn row: lines) {
			if (preSolve) {
				row.preloop_Solve(unknowns);
			}
			else {
				row.getReadyToGo();				
			}
		}
		
		// Number of unknowns AFTER
		int unkns_After = unknowns.size();
		
		// Did we make any progress
		return unkns_After != unkns_Before;
	}	
	
}
