package jcc.number_puzzle;

import java.util.ArrayList;
import java.util.List;


abstract class AbstractCell implements Comparable<Cell> {
	
	int rowIndex;
	int colIndex;
	
	final int cellNumber;
	
	List<RowOrColumn> linesIAmIn = new ArrayList<RowOrColumn>();
	
	AbstractCell(int row, int col, int cellNumb) {
		rowIndex = row;
		colIndex = col;
		cellNumber = cellNumb;
	}
	
	void addMeIntoALine(RowOrColumn line) {
		linesIAmIn.add(line);
	}
	
	@Override
	public int compareTo(Cell c) {
		if (this.equals(c)) {
				return 0;
			}
		else {
				if (rowIndex > c.rowIndex) {
					return 1;
				}
				else if (rowIndex < c.rowIndex)  {
					return -1;
				}
				else {
					if (colIndex > c.colIndex) {
						return 1;
					}
					else {
						return -1;
					}
			}
		}
	}

	public boolean equals(Object arg) {
		if (arg instanceof Cell) {
			Cell cellArg = (Cell) arg;
			return cellArg.rowIndex == rowIndex && cellArg.colIndex == colIndex; 
		}
		else {
			return false;
		}
	}

	public int hashCode() {
		return (rowIndex+1)*(colIndex+1);
	}
}
