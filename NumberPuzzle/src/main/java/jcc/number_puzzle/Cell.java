package jcc.number_puzzle;

public class Cell extends AbstractCell {

	int value = -1;

	Cell(int row, int col, int val, int cellNumb) {
		super(row, col, cellNumb);
		value = val;
	}
	boolean isKnown() {
		return value != -1;
	}
	void setSolvedValue(int toSet) throws NoValidSolutionsException {
		value = toSet;
		for (RowOrColumn rOrC: linesIAmIn) {
			rOrC.checkIsStillPossible();
			rOrC.getReadyToGo();
		}
	}
	public String toString() {
		if (isKnown()) {
			return "SolvedTestCell(" + cellNumber + ")[" + (rowIndex+1) + ", " + (colIndex+1) + "] ? TestValue{" + value + "}";
		}
		else {
			return "TestCell(" + cellNumber + ")[" + (rowIndex+1) + ", " + (colIndex+1) + "] ? TestValue{-1}";			
		}
	}
}
