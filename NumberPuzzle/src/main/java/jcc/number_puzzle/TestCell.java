package jcc.number_puzzle;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestCell extends Cell {

	static int[] POSSIBLES = {0,1,2,3,4,5,6,7,8,9}; 
	
	private int testValue = -1;
	private List<Integer> possibleValues = new ArrayList<Integer>(10);
	private int minValue = 0;
	private int maxValue = 9;

	TestCell(int row, int col, int cellNumb) {
		super(row, col, -1, cellNumb);
		for (int i = 0; i < POSSIBLES.length; i++) {
			possibleValues.add(POSSIBLES[i]);
		}
	}

	protected Iterator<Integer> iterator() {
		return possibleValues.iterator();
	}
	boolean setAndTest_TestValue(int toTest) {
		testValue = toTest;
		for (RowOrColumn rOrC: linesIAmIn) {
			if (rOrC.isReadyForCheck()) {
				boolean check = rOrC.checkSum();
				if (! check) {
					return false;
				}
			}
		}
		return true;
		
	}
	protected int maxValue() {
		return possibleValues.get(possibleValues.size()-1);
//		return maxValue;
	}
	protected int minValue() {
		return possibleValues.get(0);
//		return minValue;
	}
	
	int getTestValue() {
		return testValue;
	}
	void dumpData(StringBuilder bob, boolean dumpRange) {
		if (! isKnown()) {
			bob.append(NumberGridSolver.NEW_LINE)
				.append("\t")
				.append(this);
			if (dumpRange) {
				bob.append("  Range: ")
				.append(minValue)
				.append(">")
				.append(maxValue);
			}
		}
	}

	void resetTestValue() {
		testValue = -1;
	}
	public String toString() {
		if (isKnown()) {
			return "SolvedTestCell(" + cellNumber + ")[" + (rowIndex+1) + ", " + (colIndex+1) + "] ? TestValue{" + value + "}";
		}
		else {
			return "TestCell(" + cellNumber + ")[" + (rowIndex+1) + ", " + (colIndex+1) + "] ? TestValue{" + testValue + "}";			
		}
	}
	protected void setMaxValue(int max) throws NoValidSolutionsException {
		maxValue = Math.min(max, maxValue);
		boolean trim = possibleValues.get(possibleValues.size()-1) > maxValue;
		if (trim) {
			possibleValues.remove(possibleValues.size()-1);
			trim = possibleValues.get(possibleValues.size()-1) > minValue;
		}
		if (maxValue < minValue) {
			throw new NoValidSolutionsException("Impossible. " + this);
		}
	}
	protected void setMinValue(int min) throws NoValidSolutionsException {
		minValue = Math.max(min, minValue);
		boolean trim = possibleValues.get(0) < minValue;
		if (trim) {
			possibleValues.remove(0);
			trim = possibleValues.get(0) < minValue;
		}
		if (maxValue < minValue) {
			throw new NoValidSolutionsException("Impossible. " + this);
		}
	}

}
