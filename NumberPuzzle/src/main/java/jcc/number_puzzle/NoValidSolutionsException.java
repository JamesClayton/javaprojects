package jcc.number_puzzle;

public class NoValidSolutionsException extends Throwable {

	private static final long serialVersionUID = 1L;

	public NoValidSolutionsException(String string) {
		super(string);
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return null;
	}

}
