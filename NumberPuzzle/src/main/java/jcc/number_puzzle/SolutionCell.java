package jcc.number_puzzle;

import java.util.ArrayList;
import java.util.List;

public class SolutionCell extends AbstractCell {
	
	List<Integer> solutions = new ArrayList<Integer>();

	SolutionCell(int row, int col, int cellNumb) {
		super(row, col, cellNumb);
	}
	
	void addSolution(int sol) {
		solutions.add(sol);
	}
	public String toString() {
		StringBuilder bob = new StringBuilder();
		bob.append("SolutionCell(")
			.append(cellNumber)
			.append(")[")
			.append(rowIndex+1)
			.append(", ")
			.append(colIndex+1)
			.append("] ? Solutions{")
			.append(solutions)
			.append("}");
		if (NumberGridSolver.LOG_GEO_DATA && solutions.size() > 1) {
			int sum = 0;
			for (int sol: solutions) {
				sum = sum + sol;
			}
			bob.append("\tSUM: ")
				.append(sum);
		}
		return bob.toString();
	}

	public void addSolution(TestCell testCell) {
		if (testCell.value == -1) {
			solutions.add(testCell.getTestValue());
		}
		else {
			solutions.add(testCell.value);
		}
	}
	public int getSum() {
		int sum = 0;
		for (int sol: solutions) {
			sum = sum + sol;
		}
		return sum;
	}
}
