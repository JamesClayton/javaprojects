package jcc.number_puzzle;

import java.util.ArrayList;
import java.util.List;

class RowOrColumn implements Comparable<RowOrColumn> {
	List<Cell> cells = new ArrayList<Cell>();
	TestCell[] unknownCells;
	int expected;
	private int knownSum = -1;
	private final String rowType;
	
	private final int rowOrColumnNumber;
	final Integer number;
	
	RowOrColumn(int exp, int rowOrColNumb, String rowTyp, int numb) {
		expected = exp;
		rowOrColumnNumber = rowOrColNumb;
		rowType = rowTyp;
		number = new Integer(numb);
	}
	
	void addCell(Cell cell) {
		cells.add(cell);
		cell.addMeIntoALine(this);
	}
	
	boolean checkSum() {
		boolean check =  knownSum + unknownSum() == expected;
		return check;
	}
	@Override
	public int compareTo(RowOrColumn rc) {
		return number.compareTo(rc.number);
	}

	void dumpData(StringBuilder bob) {
		bob.append(Grid.NEW_LINE)
			.append(rowType)		
			.append(" #")
			.append(rowOrColumnNumber)
			.append(" Exp: ")
			.append(expected)
			.append(" Known: ")
			.append(knownSum)
			.append(" no of unknowns ")
			.append(numberOfUnknowns());
		if (numberOfUnknowns() > 0) {
			for (TestCell cell: unknownCells) {
				cell.dumpData(bob, false);
			}
		}
	}
	boolean isColumn() {
		return rowType.equals("Column");
	}
	boolean isDiagonal() {
		return rowType.equals("Diagonal");
	}
	boolean isRow() {
		return rowType.equals("Row");
	}
	
	int numberOfUnknowns() {
		return unknownCells.length;
	}
	void preloop_Restrict() throws NoValidSolutionsException {
		int delta = expected - knownSum;
		if (delta < 0) {
			throw new NoValidSolutionsException(rowType + ": " + rowOrColumnNumber + ". No possible solution:  The delta should never be less than zero. " + this);
		}
		if (delta < 9) {
			for (Cell cell: cells) {
				if (! cell.isKnown()) {
					((TestCell) cell).setMaxValue(delta);
				}
			}	
		}
		int uns = numberOfUnknowns();
		int min = delta - 9*(uns-1);
		if (min > 0) {
			for (Cell cell: cells) {
				if (! cell.isKnown()) {
					((TestCell) cell).setMinValue(min);
				}
			}
		}
	}
	void preloop_Solve(List<TestCell> unknowns) throws NoValidSolutionsException {
		getReadyToGo();
		int noUnknown = numberOfUnknowns();
		int delta = expected - knownSum;
		if (delta < 0) {
			throw new NoValidSolutionsException(rowType + ": " + rowOrColumnNumber + ". No possible solution: The delta should never be less than zero");
		}
		if (delta == 0) {
			for (Cell cell: cells) {
				if (! cell.isKnown()) {
					cell.setSolvedValue(0);
					unknowns.remove(cell);
					String mess = rowType + ": " + rowOrColumnNumber + " Solved a cell: " + cell + " (DELTA = 0)";
					log(mess);
				}
			}	
		}
		else if (noUnknown == 1) {
			for (Cell cell: cells) {
				if (! cell.isKnown()) {
					if (delta > 9) {
						throw new NoValidSolutionsException(rowType + ": " + rowOrColumnNumber + ". No possible solution: The cell value cannot be larger than 9");
					}
					cell.setSolvedValue(delta);
					unknowns.remove(cell);
					String mess = rowType + ": " + rowOrColumnNumber + " Solved a cell: " + cell + " (UNKNOWNS = 1)";
					log(mess);
				}
			}
		}
		else {
			noUnknown = numberOfUnknowns();
			delta = expected - knownSum;
			if (noUnknown*9 == delta) {
				for (Cell cell: cells) {
					if (! cell.isKnown()) {
						cell.setSolvedValue(9);
						unknowns.remove(cell);
						String mess = rowType + ": " + rowOrColumnNumber + " Solved a cell: " + cell+ " (UNKNOWNS*9 = DELTA)";
						log(mess);
					}
				}	
			}
		}
	}
	void log(String mess) {
		NumberGridSolver.SOLVER.log(mess);
	}
	public String toString() {
		StringBuilder bob = new StringBuilder();
		dumpData(bob);
		return bob.toString();
	}

	private int unknownSum() {
		int ret = 0;
		for (TestCell cell: unknownCells) {
			ret = ret + cell.getTestValue();
		}
		return ret;
	}

	void getReadyToGo() {
		List<Cell> myUnknowns = new ArrayList<Cell>();
		int ret = 0;
		for (Cell cell: cells) {
			if (cell.isKnown()) {
				ret = ret + cell.value;
			}
			else {
				myUnknowns.add(cell);
			}
		}
		knownSum = ret;	
		unknownCells = myUnknowns.toArray(new TestCell[myUnknowns.size()]);
		
	}

	boolean isReadyForCheck() {
		for (TestCell cell: unknownCells) {
			if (cell.getTestValue() == -1) {
				return false;
			}
		}
		return true;
	}

	public void checkIsStillPossible() throws NoValidSolutionsException {
		getReadyToGo();
		int noUnknown = numberOfUnknowns();
		int delta = expected - knownSum;
		if (delta < 0) {
			throw new NoValidSolutionsException(rowType + ": " + rowOrColumnNumber + " solution no longer possible. (delta < 0). " + this);
		}

		if (delta > noUnknown * 9) {
			throw new NoValidSolutionsException(rowType + ": " + rowOrColumnNumber + " solution no longer possible. (delta > NumberUnknowns*9). " + this);
		}
	}

}
