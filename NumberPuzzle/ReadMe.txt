JUnit test to solve number puzzles.

The puzzles are like magic squares "A magic square is a square grid filled with numbers, in such a way that each row, each column, and the two diagonals add up to the same number. "

But the rows, columns, diagonals do not all add up to the same number. The Grid contains a number of "unknown" cells - signified by "-1" Possible values for the unknowns are 0-9 inclusive.

So a row 3,4,5,-1 => 20 the unknown number would be 8

The aim of the program is to solve the puzzle & find the unknowns. The program will cope with any size grid & handles multiple solutions.